Android-App: Who Has My Stuff?
==============================

Did you ever search for a book, wondering who might have borrowed it from you?
Do you often forget how much money people are owing you?

Now you can keep track of your lent things with this very simple application. It
lets you define a description, date and contact person from your address book
for each object.

<a href="https://f-droid.org/packages/de.freewarepoint.whohasmystuff/" target="_blank">
<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="90"/></a>
<a href="https://play.google.com/store/apps/details?id=de.freewarepoint.whohasmystuff" target="_blank">
<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="90"/></a>

Features
--------

* Integration of personal address book
* History of returned objects
* Add calendar events for expected returns
* Backup to SD card
* Free, no ads, developed under an open source license

Justification for used permissions
----------------------------------

* Read address book: Associate objects with contacts
* Write to SD card: Backup/Restore

Translation
-----------

If you want to help me translating this application into your language,
please visit https://crowdin.net/project/who-has-my-stuff/invite
