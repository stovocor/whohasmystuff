package de.freewarepoint.whohasmystuff

import android.content.res.Resources
import android.text.format.DateUtils
import java.util.*

internal class DurationCalculator(private val resources: Resources) {
    fun getTimeDifference(lentDate: Calendar, now: Calendar): String {
        if (now.before(lentDate)) {
            return "0 days"
        }

        // Check if one or more years have passed
        var differenceInYears = now[Calendar.YEAR] - lentDate[Calendar.YEAR]
        val lentTimeInSameYear: Calendar = GregorianCalendar()
        lentTimeInSameYear.timeInMillis = lentDate.timeInMillis
        lentTimeInSameYear[Calendar.YEAR] = now[Calendar.YEAR]
        if (now.before(lentTimeInSameYear)) {
            differenceInYears--
        }
        if (differenceInYears > 1) {
            return differenceInYears.toString() + " " + resources.getString(R.string.years)
        } else if (differenceInYears > 0) {
            return differenceInYears.toString() + " " + resources.getString(R.string.year)
        }

        // Check if one or more months have passed
        val monthsOfLentDate = lentDate[Calendar.YEAR] * 12 + lentDate[Calendar.MONTH]
        val monthsNow = now[Calendar.YEAR] * 12 + now[Calendar.MONTH]
        var differenceInMonths = monthsNow - monthsOfLentDate
        val lentTimeInSameMonth: Calendar = GregorianCalendar()
        lentTimeInSameMonth.timeInMillis = lentDate.timeInMillis
        lentTimeInSameMonth[Calendar.YEAR] = now[Calendar.YEAR]
        lentTimeInSameMonth[Calendar.MONTH] = now[Calendar.MONTH]
        if (now.before(lentTimeInSameMonth)) {
            differenceInMonths--
        }
        if (differenceInMonths > 1) {
            return differenceInMonths.toString() + " " + resources.getString(R.string.months)
        } else if (differenceInMonths > 0) {
            return differenceInMonths.toString() + " " + resources.getString(R.string.month)
        }

        // Check if one or more weeks have passed
        val difference = now.timeInMillis - lentDate.timeInMillis
        var differenceInDays = (difference / DateUtils.DAY_IN_MILLIS).toInt()
        val differenceInWeeks = differenceInDays / 7
        if (differenceInWeeks > 1) {
            return differenceInWeeks.toString() + " " + resources.getString(R.string.weeks)
        } else if (differenceInWeeks > 0) {
            return differenceInWeeks.toString() + " " + resources.getString(R.string.week)
        }

        // Check if one or more days have passed
        val lentTimeInSameDay: Calendar = GregorianCalendar()
        lentTimeInSameDay.timeInMillis = lentDate.timeInMillis
        lentTimeInSameDay[Calendar.YEAR] = now[Calendar.YEAR]
        lentTimeInSameDay[Calendar.MONTH] = now[Calendar.MONTH]
        lentTimeInSameDay[Calendar.DAY_OF_MONTH] = now[Calendar.DAY_OF_MONTH]
        if (differenceInDays == 1 && now.before(lentTimeInSameDay)) {
            differenceInDays++
        }
        return if (differenceInDays > 1) {
            differenceInDays.toString() + " " + resources.getString(R.string.days)
        } else if (differenceInDays == 1) {
            resources.getString(R.string.yesterday)
        } else if (differenceInDays == 0) {
            if (now[Calendar.DAY_OF_MONTH] == lentDate[Calendar.DAY_OF_MONTH]) {
                resources.getString(R.string.today)
            } else {
                resources.getString(R.string.yesterday)
            }
        } else {
            resources.getString(R.string.unknown)
        }
    }
}