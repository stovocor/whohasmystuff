package de.freewarepoint.whohasmystuff.activities

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import de.freewarepoint.whohasmystuff.fragments.ListLentItems
import de.freewarepoint.whohasmystuff.R

class MainActivity : FragmentActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        if (findViewById<View?>(R.id.mainActivity) != null) {
            if (savedInstanceState != null) {
                return
            }
            val firstFragment = ListLentItems()
            firstFragment.arguments = intent.extras
            supportFragmentManager.beginTransaction()
                    .add(R.id.mainActivity, firstFragment).commit()
        }
    }

    companion object {
        const val LOG_TAG = "WhoHasMyStuff"
        const val FIRST_START = "FirstStart"
    }
}