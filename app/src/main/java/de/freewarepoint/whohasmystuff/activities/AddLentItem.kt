package de.freewarepoint.whohasmystuff.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import de.freewarepoint.whohasmystuff.R

class AddLentItem : ItemEditor() {

    private lateinit var cancelButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.add_title)
        cancelButton = findViewById(R.id.cancel_button)
        cancelButton.visibility = View.VISIBLE
        cancelButton.setOnClickListener {
            this@AddLentItem.setResult(RESULT_CANCELED, Intent())
            finish()
        }
    }

    override fun onSave(bundle: Bundle) {
    }


}