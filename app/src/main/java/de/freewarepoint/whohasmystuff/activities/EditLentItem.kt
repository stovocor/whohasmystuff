package de.freewarepoint.whohasmystuff.activities

import android.content.Intent
import android.os.Bundle
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.TextView
import de.freewarepoint.whohasmystuff.R
import de.freewarepoint.whohasmystuff.database.OpenLendDbAdapter
import de.freewarepoint.whohasmystuff.fragments.AbstractListFragment
import java.text.DateFormat
import java.util.*

class EditLentItem : ItemEditor() {

    private var rowId: Long? = null

    private lateinit var returnedButton: Button
    private lateinit var modificationDate: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.edit_title)

        saveButton.setText(R.string.edit_button)

        val bundle = intent.extras!!
        rowId = bundle.getLong(OpenLendDbAdapter.KEY_ROWID)
        returnedButton = findViewById(R.id.returned_button)
        returnedButton.visibility = VISIBLE

        modificationDate = findViewById(R.id.modification_date_text)
        modificationDate.visibility = VISIBLE

        val df = DateFormat.getDateInstance(DateFormat.LONG)
        val modificationDate = Date(bundle.getLong(OpenLendDbAdapter.KEY_MODIFICATION_DATE))
        this.modificationDate.text = getString(R.string.last_modified) + ": " + df.format(modificationDate)

        returnedButton.setOnClickListener {
            val mIntent = Intent()
            mIntent.putExtra(OpenLendDbAdapter.KEY_ROWID, rowId)
            setResult(AbstractListFragment.RESULT_RETURNED, mIntent)
            finish()
        }
    }

    override fun onSave(bundle: Bundle) {
        rowId?.let {
            bundle.putLong(OpenLendDbAdapter.KEY_ROWID, it)
        }
    }



}