package de.freewarepoint.whohasmystuff.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import de.freewarepoint.whohasmystuff.R
import de.freewarepoint.whohasmystuff.database.OpenLendDbAdapter
import de.freewarepoint.whohasmystuff.fragments.AbstractListFragment
import java.text.DateFormat
import java.util.*

class EditHistoryItem: ItemEditor() {

    private var rowId: Long? = null

    private lateinit var modificationDate: TextView
    private lateinit var deleteButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.edit_title)

        val bundle = intent.extras!!
        rowId = bundle.getLong(OpenLendDbAdapter.KEY_ROWID)
        modificationDate = findViewById(R.id.modification_date_text)
        modificationDate.visibility = View.VISIBLE

        val df = DateFormat.getDateInstance(DateFormat.LONG)
        val modificationDate = Date(bundle.getLong(OpenLendDbAdapter.KEY_MODIFICATION_DATE))
        this.modificationDate.text = getString(R.string.last_modified) + ": " + df.format(modificationDate)

        deleteButton = findViewById(R.id.delete_button)
        deleteButton.visibility = View.VISIBLE
        deleteButton.setOnClickListener {
            val mIntent = Intent()
            mIntent.putExtra(OpenLendDbAdapter.KEY_ROWID, rowId)
            setResult(AbstractListFragment.RESULT_DELETE, mIntent)
            finish()
        }

        saveButton.setText(R.string.mark_as_lent_button)
    }

    override fun onSave(bundle: Bundle) {
        rowId?.let {
            bundle.putLong(OpenLendDbAdapter.KEY_ROWID, it)
        }
    }
}