package de.freewarepoint.whohasmystuff.activities

import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.format.DateUtils
import android.view.View
import android.widget.*
import androidx.fragment.app.FragmentActivity
import de.freewarepoint.whohasmystuff.R
import de.freewarepoint.whohasmystuff.database.OpenLendDbAdapter
import de.freewarepoint.whohasmystuff.fragments.DatePickerHandler
import java.util.*

/**
 * Activity for adding a new lent item to the database
 * or editing an existing entry
 */
abstract class ItemEditor : FragmentActivity() {

    private lateinit var pickDate: Button
    private lateinit var pickReturnDate: Button
    protected lateinit var saveButton: Button
    private lateinit var descriptionText: AutoCompleteTextView
    private lateinit var personName: AutoCompleteTextView
    private lateinit var typeSpinner: Spinner

    private var originalName: String? = null
    private var originalPersonKey: String? = null
    private var selectedPersonKey: String? = null
    private var selectedDate: Date = Date()
    private var addCalendarEntry = false
    private lateinit var dbHelper: OpenLendDbAdapter

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_editor)
        setTitle(R.string.add_title)

        descriptionText = findViewById(R.id.add_description)
        typeSpinner = findViewById(R.id.type_spinner)
        personName = findViewById(R.id.personName)
        saveButton = findViewById(R.id.add_button)
        pickDate = findViewById(R.id.pickDate)
        pickReturnDate = findViewById(R.id.returnDate)

        dbHelper = OpenLendDbAdapter.getInstance(this).open()

        val typeAdapter = ArrayAdapter.createFromResource(
                this, R.array.type_array, android.R.layout.simple_spinner_item)
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        typeSpinner.adapter = typeAdapter

        intent.extras?.let { bundle ->
            if (bundle.containsKey(OpenLendDbAdapter.KEY_ROWID)) {
                initializeValuesFromBundle(bundle)
            }
        }

        val datePickerHandler = DatePickerHandler(pickDate, supportFragmentManager, selectedDate)
        val returnDate = Date(selectedDate.time + 14 * DateUtils.DAY_IN_MILLIS)
        val returnDatePickerHandler = DatePickerHandler(pickReturnDate, supportFragmentManager, returnDate)

        val selectPerson = findViewById<ImageButton>(R.id.choosePerson)
        selectPerson.setOnClickListener { selectPerson() }

        val addCalendarEntryCheckbox = findViewById<CheckBox>(R.id.add_calendar_checkbox)
        addCalendarEntryCheckbox.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
            toggleAddCalendarFields(isChecked)
        }

        descriptionText.setAdapter(descriptionsFromOtherItemsAdapter())
        personName.setAdapter(contactNameAdapter())
        saveButton.setOnClickListener {
            saveItem(datePickerHandler, returnDatePickerHandler)
        }
    }

    protected abstract fun onSave(bundle: Bundle)

    /**
     * Initializes UI elements with values from an existing item, e.g. for editing an existing one
     */
    private fun initializeValuesFromBundle(bundle: Bundle) {
        descriptionText.setText(bundle.getString(OpenLendDbAdapter.KEY_DESCRIPTION))
        typeSpinner.setSelection(bundle.getInt(OpenLendDbAdapter.KEY_TYPE))
        personName.setText(bundle.getString(OpenLendDbAdapter.KEY_PERSON))
        originalName = bundle.getString(OpenLendDbAdapter.KEY_PERSON)
        originalPersonKey = bundle.getString(OpenLendDbAdapter.KEY_PERSON_KEY)
        selectedDate = Date(bundle.getLong(OpenLendDbAdapter.KEY_DATE))
    }

    /**
     * Provides autocomplete values for the description based on previously returned objects
     */
    private fun descriptionsFromOtherItemsAdapter(): ArrayAdapter<String> {
        val descriptions = mutableSetOf<String>()
        dbHelper.fetchReturnedObjects().use {returnedItems ->
            val columnIndex = returnedItems.getColumnIndex(OpenLendDbAdapter.KEY_DESCRIPTION)
            while (returnedItems.moveToNext()) {
                descriptions.add(returnedItems.getString(columnIndex).trim { it <= ' ' })
            }
        }
        return ArrayAdapter(applicationContext, R.layout.autocomplete_select, R.id.tv_autocomplete,
                descriptions.toSortedSet().toList())
    }

    /**
     * Provides autocomplete values for contact names based on address book and existing items
     */
    private fun contactNameAdapter(): ArrayAdapter<String> {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                // We do not have permission to read contacts, do not provide autocomplete yet
                return ArrayAdapter(applicationContext, R.layout.autocomplete_select, R.id.tv_autocomplete, emptyList())
            }
        }

        val names = mutableSetOf<String>()

        // Add names from address book
        contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)?.use {c ->
            val columnIndex = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
            while (c.moveToNext()) {
                val name = c.getString(columnIndex)
                if (name != null) {
                    names.add(name.trim { it <= ' ' })
                }
            }
        }

        // Add names from history and current items
        dbHelper.fetchAllObjects().use { allItems ->
            val columnIndex = allItems.getColumnIndex(OpenLendDbAdapter.KEY_PERSON)
            while (allItems.moveToNext()) {
                names.add(allItems.getString(columnIndex).trim { it <= ' ' })
            }
        }

        return ArrayAdapter(applicationContext, R.layout.autocomplete_select, R.id.tv_autocomplete, names.toSortedSet().toList())
    }

    private fun saveItem(datePickerHandler: DatePickerHandler, returnDatePickerHandler: DatePickerHandler) {
        val saveItemBundle = Bundle()

        onSave(saveItemBundle)

        saveItemBundle.putString(OpenLendDbAdapter.KEY_DESCRIPTION, descriptionText.text.toString())
        saveItemBundle.putInt(OpenLendDbAdapter.KEY_TYPE, typeSpinner.selectedItemPosition)

        saveItemBundle.putLong(OpenLendDbAdapter.KEY_DATE, datePickerHandler.getTime())

        saveItemBundle.putString(OpenLendDbAdapter.KEY_PERSON, personName.text.toString())
        if (personName.text.toString() == originalName && selectedPersonKey == null) {
            saveItemBundle.putString(OpenLendDbAdapter.KEY_PERSON_KEY, originalPersonKey)
        } else {
            saveItemBundle.putString(OpenLendDbAdapter.KEY_PERSON_KEY, selectedPersonKey)
        }

        if (addCalendarEntry) {
            saveItemBundle.putBoolean(ADD_CALENDAR_ENTRY, true)
            saveItemBundle.putLong(RETURN_DATE, returnDatePickerHandler.getTime())
        } else {
            saveItemBundle.putBoolean(ADD_CALENDAR_ENTRY, false)
        }

        val mIntent = Intent()
        mIntent.putExtras(saveItemBundle)
        setResult(RESULT_OK, mIntent)
        finish()
    }

    public override fun onDestroy() {
        super.onDestroy()
        dbHelper.close()
    }

    private fun selectPerson() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(permission.READ_CONTACTS), REQUEST_READ_CONTACTS)
                return
            }
        }
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        startActivityForResult(intent, ACTION_SELECT_PERSON)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_READ_CONTACTS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                    startActivityForResult(intent, ACTION_SELECT_PERSON)
                }
            }
        }
    }

    override fun onActivityResult(reqCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(reqCode, resultCode, data)
        when (reqCode) {
            /* Person selection button has been clicked and the contact activity returned here */
            ACTION_SELECT_PERSON -> if (resultCode == RESULT_OK) {
                val contactData = data?.data ?: return
                contentResolver.query(contactData, null, null, null, null)?.use { c ->
                    if (c.moveToFirst()) {
                        val name = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
                        personName.setText(name)
                        selectedPersonKey = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.LOOKUP_KEY))
                    }
                }
            }
        }
    }

    private fun toggleAddCalendarFields(isChecked: Boolean) {
        addCalendarEntry = isChecked
        if (isChecked) {
            pickReturnDate.visibility = View.VISIBLE
            findViewById<View>(R.id.return_date_text).visibility = View.VISIBLE
        } else {
            pickReturnDate.visibility = View.GONE
            findViewById<View>(R.id.return_date_text).visibility = View.GONE
        }
    }


    companion object {
        const val ADD_CALENDAR_ENTRY = "add_calendar_entry"
        const val RETURN_DATE = "return_date"
        const val ACTION_SELECT_PERSON = 3
        const val REQUEST_READ_CONTACTS = 1024
    }
}