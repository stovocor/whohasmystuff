package de.freewarepoint.whohasmystuff.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.FragmentManager
import java.text.DateFormat
import java.util.*


class DatePickerHandler(private val selectButton: Button, private val fragmentManager: FragmentManager,
                        initialDate: Date): View.OnClickListener {

    private var year = 0
    private var month = 0
    private var day = 0

    init {
        selectButton.setOnClickListener(this)
        val c = Calendar.getInstance()
        c.time = initialDate
        year = c[Calendar.YEAR]
        month = c[Calendar.MONTH]
        day = c[Calendar.DAY_OF_MONTH]
        updateDisplay()
    }

    private fun updateDisplay() {
        val df = DateFormat.getDateInstance(DateFormat.LONG)
        val c = Calendar.getInstance()
        c[year, month] = day
        selectButton.text = df.format(c.time)
    }

    fun getTime(): Long {
        val c = Calendar.getInstance()
        c[year, month] = day
        return c.time.time
    }

    override fun onClick(v: View?) {
        val bundle = Bundle()
        bundle.putInt("year", year)
        bundle.putInt("month", month)
        bundle.putInt("day", day)

        val onUpdate: (Int, Int, Int) -> Unit = {
            y, m, d ->
                year = y
                month = m
                day = d
                updateDisplay()
        }

        val pickDateDialog = DatePickerFragment(onUpdate)
        pickDateDialog.arguments = bundle
        pickDateDialog.show(fragmentManager, "fragment_pick_date")
    }

}