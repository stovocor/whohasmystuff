package de.freewarepoint.whohasmystuff.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment

/**
 * A `DialogFragment` tailored to manage `AlertDialog`s.
 * The object interested in responding to the dialog actions must implement the inner interface
 * [AlertDialogFragment.AlertDialogFragmentListener].
 * The dialog can be slightly customized by passing some information in a Bundle via
 * `setArguments(Bundle)`, using this class constants as keys; or, equivalently,
 * by means of the class method `newObject()`.
 *
 * Since DialogFragments must call the method `show()` in order to show the dialog,
 * and that method requires a tag to identify the fragment, **the listener can identify
 * dialogs by means of their tag**.
 *
 * Example of how to use this class:
 * <pre>`
 * ...
 * Bundle params;
 * AlertDialogFragment dialog;
 *
 * //put dialog parameters in the bundle
 * params = new Bundle();
 * params.putString(AlertDialogFragment.DIALOG_TITLE, "my cool dialog title");
 * params.putString(AlertDialogFragment.DIALOG_MESSAGE, "my cool dialog message");
 * params.putString(AlertDialogFragment.DIALOG_POSITIVE_TEXT, "Ok");
 * params.putString(AlertDialogFragment.DIALOG_NEGATIVE_TEXT, "Cancel");
 *
 * //create the dialog and pass the construction parameters
 * dialog = new AlertDialogFragment();
 * dialog.setArguments(params);
 * dialog.setAlertDialogFragmentListener(getActivity());
 * dialog.show(getFragmentManager(), "fragmentTag");
 * ...
`</pre> *
 *
 * That piece of code would show a dialog with two buttons; which is equivalent to the following code
 *
 * <pre>`
 * AlertDialogFragment dialog = AlertDialogFragment.newObject("my cool dialog title",
 * "my cool dialog message", "Ok", "Cancel", null, 0);
 * dialog.setAlertDialogFragmentListener(getActivity());
 * dialog.show(getFragmentManager(), "fragmentTag");
`</pre> *
 *
 * The related listener (an activity in this case) could look more or less like this:
 *
 * <pre>`
 * public class MainActivity extends AppCompatActivity
 * implements AlertDialogFragment.AlertDialogFragmentListener{
 * public static final String LOG_TAG = "MainActivity";
 *
 * ...
 * @Override
 * public void onPositiveAction(DialogFragment dialog){
 * String tag = dialog.getTag();
 *
 * switch(tag){
 * case "desired tag":
 * //your code goes here
 * break;
 * case "another desired tag":
 * //your code goes here
 * break;
 * ...
 * }
 * }
 *
 * @Override
 * public void onNegativeAction(DialogFragment dialog){
 * String tag = dialog.getTag();
 *
 * switch(tag){
 * case "desired tag":
 * //your code goes here
 * break;
 * case "another desired tag":
 * //your code goes here
 * break;
 * ...
 * }
 * }
 *
 * @Override
 * public void onNeutralAction(DialogFragment dialog){
 * String tag = dialog.getTag();
 *
 * switch(tag){
 * case "desired tag":
 * //your code goes here
 * break;
 * case "another desired tag":
 * //your code goes here
 * break;
 * ...
 * }
 * }
 * }
`</pre> *
 *
 * To inform the user about an error we can create a dialog with just one button:
 *
 * <pre>`
 * ...
 * Bundle params;
 * AlertDialogFragment dialog;
 *
 * //put dialog parameters in the bundle
 * params = new Bundle(4);
 * params.putString(AlertDialogFragment.DIALOG_TITLE, "my error dialog");
 * params.putString(AlertDialogFragment.DIALOG_MESSAGE, "super dangerous error happened");
 * params.putString(AlertDialogFragment.DIALOG_POSITIVE_TEXT, "Understood");
 * params.putInt(AlertDialogFragment.DIALOG_ICON, android.R.drawable.ic_alert_dialog);
 *
 * //create the dialog and pass the construction parameters
 * dialog = new AlertDialogFragment();
 * dialog.setArguments(params);
 * dialog.setAlertDialogFragmentListener(someObject);
 * dialog.show(getFragmentManager(), "exampleTag");
 * ...
`</pre> *
 *
 * This is equivalent to:
 *
 * <pre>`
 * AlertDialogFragment dialog = AlertDialogFragment.newObject("my error dialog",
 * "super dangerous error happened", "Understood", null,
 * null, android.R.drawable.ic_alert_dialog);
 * dialog.setAlertDialogFragmentListener(someObject);
 * dialog.show(getFragmentManager(), "exampleTag");
`</pre> *
 * @author trikaphundo (d3vS4n@tutanota.com)
 */
class AlertDialogFragment : DialogFragment() {
    /**The observer (listener) for this fragment's dialog actions. */
    private var mListener: AlertDialogFragmentListener? = null

    /**
     * Interface for listening to actions of the AlertDialog held by an AlertDialogFragment.
     * The object interested in an AlertDialog's actions should implement this interface in order to
     * run the appropriate code when the user makes (clicks) an action of the dialog.
     * Since DialogFragments must call the method `show()` in order to show the dialog,
     * and that method requires a tag to identify the fragment, **the listener can identify
     * dialogs by means of their tag**.
     */
    interface AlertDialogFragmentListener {
        /**
         * Code to run when the user clicks the positive action of an AlertDialog.
         * @param dialog the dialog which received the click.
         */
        fun onPositiveAction(dialog: DialogFragment)

        /**
         * Code to run when the user clicks the negative action of an AlertDialog.
         * @param dialog the dialog which received the click.
         */
        fun onNegativeAction(dialog: DialogFragment?)

        /**
         * Code to run when the user clicks the neutral action of an AlertDialog.
         * @param dialog the dialog which received the click
         */
        fun onNeutralAction(dialog: DialogFragment?)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val onClickListener: DialogInterface.OnClickListener


        //retrieve constructor arguments; if none, use default values
        val dialogParams: Bundle? = arguments
        val title: String?
        val msg: String?
        val positiveText: String?
        val negativeText: String?
        val neutralText: String?
        val iconId: Int
        if (dialogParams != null) {
            title = dialogParams.getString(DIALOG_TITLE)
            msg = dialogParams.getString(DIALOG_MESSAGE)
            positiveText = dialogParams.getString(DIALOG_POSITIVE_TEXT)
            negativeText = dialogParams.getString(DIALOG_NEGATIVE_TEXT)
            neutralText = dialogParams.getString(DIALOG_NEUTRAL_TEXT)
            iconId = dialogParams.getInt(DIALOG_ICON)
        } else { //default values
            neutralText = null
            negativeText = neutralText
            positiveText = negativeText
            msg = positiveText
            title = msg
            iconId = 0
        }


        //create the listener for the dialog buttons,
        // just a man in the middle to call the listener methods
        onClickListener = DialogInterface.OnClickListener { _, which ->
            mListener?.let {
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> it.onPositiveAction(this@AlertDialogFragment)
                    DialogInterface.BUTTON_NEGATIVE -> it.onNegativeAction(this@AlertDialogFragment)
                    DialogInterface.BUTTON_NEUTRAL -> it.onNeutralAction(this@AlertDialogFragment)
                }
            }
        }


        //create the dialog
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
                .setMessage(msg)
                .setPositiveButton(positiveText, onClickListener)
                .setNegativeButton(negativeText, onClickListener)
                .setNeutralButton(neutralText, onClickListener)
        if (iconId != 0) { //is valid resource identifier
            builder.setIcon(iconId)
        }
        return builder.create()
    }

    /**
     * Sets the dialog's listener.
     * When the user makes (clicks) an action on the dialog, the listener will be notified.
     * It is **important** to call this method *before* adding this fragment
     * to a FragmentManager.
     * @param l listener of this fragment's dialog actions
     */
    fun setAlertDialogFragmentListener(l: AlertDialogFragmentListener?) {
        mListener = l
    }

    companion object {
        /**Key to identify the dialog's title in the Bundle argument. The mapped value is a String */
        const val DIALOG_TITLE = "dialog_title"

        /**Key to identify the message of the dialog in the Bundle argument. The mapped value is a String */
        const val DIALOG_MESSAGE = "dialog_message"

        /**Key to identify the text of the dialog's positive action in the Bundle argument. The mapped value is a String */
        const val DIALOG_POSITIVE_TEXT = "dialog_positive_text"

        /**Key to identify the text of the dialog's negative action in the Bundle argument. The mapped value is a String */
        const val DIALOG_NEGATIVE_TEXT = "dialog_negative_text"

        /**Key to identify the text of the dialog's neutral action in the Bundle argument. The mapped value is a String */
        const val DIALOG_NEUTRAL_TEXT = "dialog_neutral_text"

        /**Key to identify in the Bundle argument the icon to use on the dialog. The mapped value is a resource id */
        const val DIALOG_ICON = "dialog_icon"

        /**
         * Convenient class method to create objects of this class.
         * This method saves you the creation of the Bundle holding the supplied parameters;
         * This method is equivalent to:
         * <pre>`
         * AlertDialogFragment dialog = new AlertDialogFragment();
         * Bundle params = new Bundle();
         *
         * params.putString(AlertDialogFragment.DIALOG_TITLE, title);
         * params.putString(AlertDialogFragment.DIALOG_MESSAGE, message);
         * params.putString(AlertDialogFragment.DIALOG_POSITIVE_TEXT, positiveText);
         * params.putString(AlertDialogFragment.DIALOG_NEGATIVE_TEXT, negativeText);
         * params.putString(AlertDialogFragment.DIALOG_NEUTRAL_TEXT, neutralText);
         * params.putInt(AlertDialogFragment.DIALOG_ICON, iconId);
         *
         * dialog.setArguments(params);
        `</pre> *
         * @param title dialog's title
         * @param message dialog's message
         * @param positiveText text for the positive action of the dialog
         * @param negativeText text for the negative action of the dialog
         * @param neutralText text for the neutral action of the dialog
         * @param iconId drawable resource identifier, 0 if none
         * @return an object of this class with the given parameters set
         */
        fun newObject(title: String?, message: String?,
                      positiveText: String?, negativeText: String?,
                      neutralText: String?, iconId: Int): AlertDialogFragment {
            val params = Bundle(6)
            params.putString(DIALOG_TITLE, title)
            params.putString(DIALOG_MESSAGE, message)
            params.putString(DIALOG_POSITIVE_TEXT, positiveText)
            params.putString(DIALOG_NEGATIVE_TEXT, negativeText)
            params.putString(DIALOG_NEUTRAL_TEXT, neutralText)
            params.putInt(DIALOG_ICON, iconId)
            val dialog = AlertDialogFragment()
            dialog.arguments = params
            return dialog
        }
    }
}