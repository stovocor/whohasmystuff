package de.freewarepoint.whohasmystuff.fragments

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import de.freewarepoint.whohasmystuff.activities.ItemEditor

class DatePickerFragment(private val onUpdate: (Int, Int, Int) -> Unit) : DialogFragment(), OnDateSetListener {

    private var year = 0
    private var month = 0
    private var day = 0

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)

        args?.let {
            year = it.getInt("year")
            month = it.getInt("month")
            day = it.getInt("day")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return DatePickerDialog(requireActivity(), this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        onUpdate.invoke(year, month, day)
    }
}