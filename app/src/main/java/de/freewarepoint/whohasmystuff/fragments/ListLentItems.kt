package de.freewarepoint.whohasmystuff.fragments

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.mikepenz.aboutlibraries.LibsBuilder
import de.freewarepoint.whohasmystuff.R
import de.freewarepoint.whohasmystuff.activities.AddLentItem
import de.freewarepoint.whohasmystuff.activities.EditLentItem
import de.freewarepoint.whohasmystuff.database.DatabaseHelper.exportDatabaseToXML
import de.freewarepoint.whohasmystuff.database.DatabaseHelper.importDatabaseFromXML
import de.freewarepoint.whohasmystuff.fragments.AlertDialogFragment.AlertDialogFragmentListener
import java.text.SimpleDateFormat
import java.util.*

class ListLentItems : AbstractListFragment(), AlertDialogFragmentListener {

    override val editAction = EditLentItem::class.java
    override val redirectToDefaultListAfterEdit = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().setTitle(R.string.app_name)
        setHasOptionsMenu(true)
    }

    override fun getDisplayedObjects(): Cursor {
        return dbHelper.fetchLentObjects()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        view: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        menu.add(Menu.NONE, SUBMENU_EDIT, Menu.NONE, R.string.submenu_edit)
        menu.add(Menu.NONE, SUBMENU_MARK_AS_RETURNED, Menu.NONE, R.string.submenu_mark_as_returned)
        menu.add(Menu.NONE, SUBMENU_DELETE, Menu.NONE, R.string.submenu_delete)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i: Intent
        when (item.itemId) {
            R.id.addButton -> {
                i = Intent(activity, AddLentItem::class.java)
                startActivityForResult(i, ACTION_ADD)
            }
            R.id.historyButton -> {
                val newFragment: Fragment = ShowHistory()
                val transaction = requireFragmentManager().beginTransaction()
                transaction.replace(R.id.mainActivity, newFragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
            R.id.exportButton -> exportData()
            R.id.importButton -> askForImportConfirmation()
            R.id.licensesButton -> LibsBuilder().start(this.requireContext())
        }
        return true
    }

    override fun onPositiveAction(dialog: DialogFragment) {
        val tag = dialog.tag ?: return
        when (tag) {
            TAG_DIALOG_IMPORT -> importData()
        }
    }

    override fun onNegativeAction(dialog: DialogFragment?) {}
    override fun onNeutralAction(dialog: DialogFragment?) {}

    private fun exportData() {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/xml"
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ROOT)
            val date = dateFormat.format(Date())
            putExtra(Intent.EXTRA_TITLE, "WhoHasMyStuff-${date}.xml")
        }
        startActivityForResult(intent, EXPORT_TO_FILE)
    }

    private fun askForImportConfirmation() {
        val dialog: AlertDialogFragment = AlertDialogFragment.newObject(
            resources.getString(R.string.database_import_title),
            resources.getString(R.string.database_import_message),
            resources.getString(android.R.string.ok),
            resources.getString(android.R.string.cancel),
            null,
            android.R.drawable.ic_dialog_alert
        )
        dialog.setAlertDialogFragmentListener(this)
        dialog.show(parentFragmentManager, TAG_DIALOG_IMPORT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data ?: return

        if (resultCode != Activity.RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data)
            return
        }

        val uri = data.data
        val resolver = requireActivity().contentResolver

        if (requestCode == EXPORT_TO_FILE && uri != null) {
            if (exportDatabaseToXML(dbHelper, resolver.openOutputStream(uri))) {
                Toast.makeText(activity, R.string.database_export_success, Toast.LENGTH_LONG).show()
            } else {
                showExportErrorDialog()
            }
        } else if (requestCode == SELECT_IMPORT_FILE && uri != null) {
            if (importDatabaseFromXML(dbHelper, resolver.openInputStream(uri))) {
                loaderManagerInstance.restartLoader(0, null, this)
            } else {
                showImportErrorDialog()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun importData() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/xml"
        }

        startActivityForResult(intent, SELECT_IMPORT_FILE)
    }

    private fun showImportErrorDialog() {
        showErrorDialog(getString(R.string.database_import_error))
    }

    private fun showExportErrorDialog() {
        showErrorDialog(getString(R.string.database_export_error))
    }

    /**
     * Shows an AlertDialog with the given message
     * @param message the text to display
     */
    private fun showErrorDialog(message: String) {

        //set dialog args
        val args = Bundle(4)
        args.putString(AlertDialogFragment.DIALOG_TITLE, resources.getString(R.string.database_import_title))
        args.putString(AlertDialogFragment.DIALOG_MESSAGE, message)
        args.putString(AlertDialogFragment.DIALOG_POSITIVE_TEXT, resources.getString(android.R.string.ok))
        args.putInt(AlertDialogFragment.DIALOG_ICON, android.R.drawable.ic_dialog_alert)

        val dialog = AlertDialogFragment()
        dialog.arguments = args
        dialog.setAlertDialogFragmentListener(this)
        dialog.show(parentFragmentManager, null)
    }

    companion object {
        /**Tag used to identify the DialogFragment for import confirmation. */
        private const val TAG_DIALOG_IMPORT = "import_confirmation_dialog"
    }
}