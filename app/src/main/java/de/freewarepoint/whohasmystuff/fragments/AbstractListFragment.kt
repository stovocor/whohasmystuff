package de.freewarepoint.whohasmystuff.fragments

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.CalendarContract
import android.provider.CalendarContract.Events
import android.util.Log
import android.view.MenuItem
import android.view.SubMenu
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.AdapterView.OnItemClickListener
import android.widget.SimpleCursorAdapter
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.ListFragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import de.freewarepoint.whohasmystuff.DurationCalculator
import de.freewarepoint.whohasmystuff.LentObject
import de.freewarepoint.whohasmystuff.R
import de.freewarepoint.whohasmystuff.activities.ItemEditor
import de.freewarepoint.whohasmystuff.activities.MainActivity
import de.freewarepoint.whohasmystuff.database.OpenLendDbAdapter
import de.freewarepoint.whohasmystuff.fragments.AlertDialogFragment.AlertDialogFragmentListener
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Fragment listing items from the database, either lent items (ListLentItems)
 * or returned items (ShowHistory)
 */
abstract class AbstractListFragment : ListFragment(), AlertDialogFragmentListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    protected lateinit var dbHelper: OpenLendDbAdapter

    protected lateinit var loaderManagerInstance: LoaderManager

    private lateinit var lentObjectAdapter: SimpleCursorAdapter

    private val from = arrayOf(
            OpenLendDbAdapter.KEY_DESCRIPTION,
            OpenLendDbAdapter.KEY_PERSON,
            OpenLendDbAdapter.KEY_DATE,
            OpenLendDbAdapter.KEY_MODIFICATION_DATE
    )
    private val to = intArrayOf(
            R.id.toptext,
            R.id.bottomtext,
            R.id.date
    )

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = requireActivity()

        dbHelper = OpenLendDbAdapter.getInstance(activity)
        dbHelper.open()

        loaderManagerInstance = LoaderManager.getInstance(this)
        loaderManagerInstance.initLoader(0, null, this)
        lentObjectAdapter = SimpleCursorAdapter(this.activity, R.layout.row, null, from, to, 0)
        listAdapter = lentObjectAdapter

        listView.onItemClickListener = OnItemClickListener { _: AdapterView<*>?, _: View?, position: Int, id: Long -> launchEditActivity(position, id) }
        registerForContextMenu(listView)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        return object: CursorLoader(requireContext()) {
            override fun loadInBackground(): Cursor {
                return getDisplayedObjects()
            }
        }
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor?) {
        lentObjectAdapter.swapCursor(data)

        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT)
        val now: Calendar = GregorianCalendar()
        val durationCalculator = DurationCalculator(resources)
        lentObjectAdapter.viewBinder = SimpleCursorAdapter.ViewBinder { view: View, cursor: Cursor, columnIndex: Int ->
            if (columnIndex == 3) {
                val date = cursor.getString(columnIndex)
                val time = df.parse(date)!!.time
                val lentDate: Calendar = GregorianCalendar()
                lentDate.timeInMillis = time
                val dateView = view.findViewById<View>(R.id.date) as TextView
                dateView.text = durationCalculator.getTimeDifference(lentDate, now)
                return@ViewBinder true
            }
            false
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        lentObjectAdapter.swapCursor(null)
    }

    /**
     * The type of action called when an item is edited (edit lent item or edit returned item)
     */
    protected abstract val editAction: Class<out ItemEditor>

    protected abstract val redirectToDefaultListAfterEdit: Boolean

    private fun launchEditActivity(position: Int, id: Long) {
        val cursor = lentObjectAdapter.cursor
        cursor.moveToPosition(position)
        val extras = Bundle()
        extras.putLong(OpenLendDbAdapter.KEY_ROWID, id)
        extras.putString(OpenLendDbAdapter.KEY_DESCRIPTION, cursor.getString(
                cursor.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_DESCRIPTION)))
        extras.putInt(OpenLendDbAdapter.KEY_TYPE, cursor.getInt(
                cursor.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_TYPE)))
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT)
        val date =
            df.parse(cursor.getString(cursor.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_DATE)))
                ?: Date()
        extras.putLong(OpenLendDbAdapter.KEY_DATE, date.time)
        val modificationDate =
            df.parse(cursor.getString(cursor.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_MODIFICATION_DATE)))
                ?: Date()
        extras.putLong(OpenLendDbAdapter.KEY_MODIFICATION_DATE, modificationDate.time)
        extras.putString(OpenLendDbAdapter.KEY_PERSON, cursor.getString(
                cursor.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_PERSON)))
        extras.putString(OpenLendDbAdapter.KEY_PERSON_KEY, cursor.getString(
                cursor.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_PERSON_KEY)))
        val intent = Intent(activity, editAction)
        intent.action = Intent.ACTION_EDIT
        intent.putExtras(extras)
        startActivityForResult(intent, ACTION_EDIT)
    }

    /**
     * Returns the item to be displayed (lent items or returned items)
     */
    protected abstract fun getDisplayedObjects(): Cursor

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = try {
            item.menuInfo as AdapterContextMenuInfo
        } catch (e: ClassCastException) {
            Log.e(LOG_TAG, "Bad MenuInfo", e)
            return false
        }

        val id = listAdapter!!.getItemId(info.position)
        if (item.itemId == SUBMENU_EDIT || item.itemId == SUBMENU_LEND_AGAIN) {
            launchEditActivity(info.position, id)
        } else if (item.itemId == SUBMENU_MARK_AS_RETURNED) {
            dbHelper.markLentObjectAsReturned(id)
            loaderManagerInstance.restartLoader(0, null, this)
        } else if (item.itemId == SUBMENU_DELETE) {
            dbHelper.deleteLentObject(id)
            loaderManagerInstance.restartLoader(0, null, this)
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data ?: return
        val bundle = data.extras ?: return

        if (resultCode == Activity.RESULT_OK) {
            val lentObject = LentObject(bundle)

            if (bundle.getBoolean(ItemEditor.ADD_CALENDAR_ENTRY)) {
                addCalendarEntry(bundle.getLong(ItemEditor.RETURN_DATE), lentObject)
            }

            if (requestCode == ACTION_ADD) {
                lentObject.returned = false
                lentObject.modificationDate = Date()
                dbHelper.createLentObject(lentObject)
            } else if (requestCode == ACTION_EDIT) {
                val rowId = bundle.getLong(OpenLendDbAdapter.KEY_ROWID)
                dbHelper.updateLentObject(rowId, lentObject)
                dbHelper.markReturnedObjectAsLentAgain(rowId)
                if (redirectToDefaultListAfterEdit) {
                    val intent = Intent(activity, MainActivity::class.java)
                    startActivity(intent)
                }
            }
        } else if (resultCode == RESULT_DELETE) {
            val rowId = bundle.getLong(OpenLendDbAdapter.KEY_ROWID)
            dbHelper.deleteLentObject(rowId)
        } else if (resultCode == RESULT_RETURNED) {
            val rowId = bundle.getLong(OpenLendDbAdapter.KEY_ROWID)
            dbHelper.markLentObjectAsReturned(rowId)
        }

        loaderManagerInstance.restartLoader(0, null, this)
    }

    private fun addCalendarEntry(startTime: Long, lentObject: LentObject) {
        val title = String.format(getString(R.string.expected_return), lentObject.description)
        val description = String.format(
                getString(R.string.calendar_description), lentObject.description.trim { it <= ' ' }, lentObject.personName.trim { it <= ' ' })
        val intent = Intent(Intent.ACTION_EDIT)
                .setType("vnd.android.cursor.item/event")
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
                .putExtra(Events.ALL_DAY, true)
                .putExtra(Events.TITLE, title)
                .putExtra(Events.DESCRIPTION, description)
        startActivity(intent)
    }

    override fun onNegativeAction(dialog: DialogFragment?) {}
    override fun onNeutralAction(dialog: DialogFragment?) {}

    companion object {
        const val SUBMENU_EDIT = SubMenu.FIRST
        const val SUBMENU_MARK_AS_RETURNED = SubMenu.FIRST + 1
        const val SUBMENU_LEND_AGAIN = SubMenu.FIRST + 2
        const val SUBMENU_DELETE = SubMenu.FIRST + 3
        const val ACTION_ADD = 1
        const val ACTION_EDIT = 2
        const val RESULT_DELETE = 2
        const val RESULT_RETURNED = 3
        const val EXPORT_TO_FILE = 4
        const val SELECT_IMPORT_FILE = 5
        const val LOG_TAG = "WhoHasMyStuff"
    }
}