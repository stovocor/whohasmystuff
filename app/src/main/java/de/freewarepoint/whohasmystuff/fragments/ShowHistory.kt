package de.freewarepoint.whohasmystuff.fragments

import android.database.Cursor
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.View
import androidx.fragment.app.DialogFragment
import de.freewarepoint.whohasmystuff.R
import de.freewarepoint.whohasmystuff.activities.EditHistoryItem

class ShowHistory : AbstractListFragment() {

    override val editAction = EditHistoryItem::class.java

    override val redirectToDefaultListAfterEdit = true

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().setTitle(R.string.history_title)
        setHasOptionsMenu(false)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        view: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        menu.add(Menu.NONE, SUBMENU_EDIT, Menu.NONE, R.string.submenu_edit)
        menu.add(Menu.NONE, SUBMENU_LEND_AGAIN, Menu.NONE, R.string.mark_as_lent_button)
        menu.add(Menu.NONE, SUBMENU_DELETE, Menu.NONE, R.string.submenu_delete)
    }

    override fun onPositiveAction(dialog: DialogFragment) {

    }

    override fun getDisplayedObjects(): Cursor {
        return dbHelper.fetchReturnedObjects()
    }

}