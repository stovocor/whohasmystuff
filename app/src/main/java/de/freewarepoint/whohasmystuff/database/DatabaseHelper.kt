package de.freewarepoint.whohasmystuff.database

import android.util.Log
import de.freewarepoint.whohasmystuff.activities.MainActivity
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import org.xml.sax.helpers.XMLReaderFactory
import java.io.*
import java.nio.charset.StandardCharsets
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DatabaseHelper {
    fun exportDatabaseToXML(database: OpenLendDbAdapter, output: OutputStream?): Boolean {
        output ?: return false

        try {
            BufferedWriter(
                OutputStreamWriter(
                    output,
                    StandardCharsets.UTF_8
                )
            ).use {
                it.write(convertDatabaseToXml(database))
            }
        } catch (e: ParseException) {
            Log.e(MainActivity.LOG_TAG, "Error while exporting to XML", e)
            return false
        } catch (e: IOException) {
            Log.e(MainActivity.LOG_TAG, "Error while exporting to XML", e)
            return false
        }
        return true
    }

    @Throws(ParseException::class)
    private fun convertDatabaseToXml(database: OpenLendDbAdapter): String {
        val c = database.fetchAllObjects()
        val sb = StringBuilder()
        sb.append("""<?xml version="1.0" encoding="UTF-8"?>""")
        sb.append("\n")
        sb.append("""<DatabaseBackup version="${OpenLendDbAdapter.DATABASE_VERSION}">""")
        sb.append("\n")
        if (c.count > 0) {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT)
            c.moveToFirst()
            while (!c.isAfterLast) {
                sb.append("<LentObject")
                val description = c.getString(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_DESCRIPTION))
                sb.append(" description=\"").append(replace(description)).append("\"")
                val type = c.getInt(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_TYPE))
                sb.append(" type=\"").append(type).append("\"")
                val date =
                    df.parse(c.getString(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_DATE)))
                        ?: Date()
                sb.append(" date=\"").append(date.time).append("\"")
                val modificationDate =
                    df.parse(c.getString(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_MODIFICATION_DATE)))
                        ?: Date()
                sb.append(" modificationDate=\"").append(modificationDate.time).append("\"")
                val personName = c.getString(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_PERSON))
                sb.append(" personName=\"").append(replace(personName)).append("\"")
                val personKey = c.getString(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_PERSON_KEY))
                sb.append(" personKey=\"").append(personKey).append("\"")
                val back = c.getInt(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_BACK))
                sb.append(" returned=\"").append(back).append("\"")
                val calendarUri = c.getString(c.getColumnIndexOrThrow(OpenLendDbAdapter.KEY_CALENDAR_ENTRY))
                sb.append(" calendarEvent=\"").append(calendarUri).append("\"")
                sb.append("/>\n")
                c.moveToNext()
            }
        }
        sb.append("</DatabaseBackup>")
        return sb.toString()
    }

    private fun replace(value: String): String {
        var escapedString = value
        escapedString = escapedString.replace("&", "&amp;")
        escapedString = escapedString.replace("\"", "&quot;")
        escapedString = escapedString.replace("<", "&lt;")
        escapedString = escapedString.replace(">", "&gt;")
        escapedString = escapedString.replace("'", "&apos;")
        return escapedString
    }

    fun importDatabaseFromXML(database: OpenLendDbAdapter, input: InputStream?): Boolean {
        input ?: return false

        val contentHandler = XMLContentHandler()
        System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver")
        try {
            BufferedReader(InputStreamReader(input)).use {
                val source = InputSource(it)
                val myReader = XMLReaderFactory.createXMLReader()
                myReader.contentHandler = contentHandler
                myReader.parse(source)
            }
        } catch (e: SAXException) {
            Log.e(MainActivity.LOG_TAG, "Error while parsing XML", e)
            return false
        } catch (e: IOException) {
            Log.e(MainActivity.LOG_TAG, "Error while parsing XML", e)
            return false
        }
        database.clearDatabase()
        for (lentObject in contentHandler.lentObjects) {
            database.createLentObject(lentObject)
        }
        return true
    }

}