/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.freewarepoint.whohasmystuff.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import de.freewarepoint.whohasmystuff.LentObject
import de.freewarepoint.whohasmystuff.fragments.ListLentItems
import de.freewarepoint.whohasmystuff.activities.MainActivity
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class OpenLendDbAdapter private constructor(private val mCtx: Context) {
    private lateinit var mDbHelper: DatabaseHelper
    private lateinit var mDb: SQLiteDatabase

    private class DatabaseHelper(val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase) {
            db.execSQL(LENTOBJECTS_DATABASE_CREATE)
            val preferences = context.getSharedPreferences(ListLentItems::class.java.simpleName, Context.MODE_PRIVATE)
            val editor = preferences.edit()
            editor.putBoolean(MainActivity.FIRST_START, true)
            editor.apply()
        }

        fun createWithoutExampleData(db: SQLiteDatabase) {
            db.execSQL(LENTOBJECTS_DATABASE_CREATE)
        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            Log.i(MainActivity.LOG_TAG, "Upgrading database from version $oldVersion to $newVersion")
            if (oldVersion < 2) {
                db.execSQL(CREATE_CALENDAR_ENTRY_COLUMN)
            }
            if (oldVersion < 3) {
                db.execSQL(CREATE_TYPE_COLUMN)
            }
            if (oldVersion < 4) {
                db.execSQL(CREATE_MODIFICATION_DATE_COLUMN)
                db.execSQL(COPY_DATES)
            }
        }
    }

    fun open(): OpenLendDbAdapter {
        mDbHelper = DatabaseHelper(mCtx)
        mDb = mDbHelper.writableDatabase
        return this
    }

    fun close() {
        mDbHelper.close()
    }

    fun clearDatabase() {
        mDb.execSQL("DROP TABLE IF EXISTS lentobjects")
        mDbHelper.createWithoutExampleData(mDb)
    }

    fun createLentObject(lentObject: LentObject?): Long {
        val initialValues = ContentValues()
        initialValues.put(KEY_DESCRIPTION, lentObject!!.description)
        initialValues.put(KEY_TYPE, lentObject.type)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT)
        initialValues.put(KEY_DATE, dateFormat.format(lentObject.date))
        initialValues.put(KEY_MODIFICATION_DATE, dateFormat.format(lentObject.modificationDate))
        initialValues.put(KEY_PERSON, lentObject.personName)
        initialValues.put(KEY_PERSON_KEY, lentObject.personKey)
        initialValues.put(KEY_BACK, lentObject.returned)
        if (lentObject.calendarEventURI != null) {
            initialValues.put(KEY_CALENDAR_ENTRY, lentObject.calendarEventURI.toString())
        }
        return mDb.insert(LENTOBJECTS_DATABASE_TABLE, null, initialValues)
    }

    fun deleteLentObject(rowId: Long): Boolean {
        return mDb.delete(LENTOBJECTS_DATABASE_TABLE, "$KEY_ROWID=$rowId", null) > 0
    }

    fun fetchAllObjects(): Cursor {
        return mDb.query(LENTOBJECTS_DATABASE_TABLE, arrayOf(KEY_ROWID,
                KEY_DESCRIPTION, KEY_TYPE, KEY_DATE, KEY_MODIFICATION_DATE, KEY_PERSON, KEY_PERSON_KEY, KEY_BACK, KEY_CALENDAR_ENTRY), null, null, null, null, "$KEY_DATE ASC")
    }

    fun fetchLentObjects(): Cursor {
        return mDb.query(LENTOBJECTS_DATABASE_TABLE, arrayOf(KEY_ROWID,
                KEY_DESCRIPTION, KEY_TYPE, KEY_DATE, KEY_MODIFICATION_DATE, KEY_PERSON, KEY_PERSON_KEY, KEY_BACK, KEY_CALENDAR_ENTRY), "$KEY_BACK=0", null, null, null, "$KEY_DATE ASC")
    }

    fun fetchReturnedObjects(): Cursor {
        return mDb.query(LENTOBJECTS_DATABASE_TABLE, arrayOf(KEY_ROWID,
                KEY_DESCRIPTION, KEY_TYPE, KEY_DATE, KEY_MODIFICATION_DATE, KEY_PERSON, KEY_PERSON_KEY, KEY_BACK, KEY_CALENDAR_ENTRY), "$KEY_BACK=1", null, null, null, "$KEY_DATE ASC")
    }

    fun updateLentObject(rowId: Long, lentObject: LentObject): Boolean {
        val args = ContentValues()
        args.put(KEY_DESCRIPTION, lentObject.description)
        args.put(KEY_TYPE, lentObject.type)
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT)
        args.put(KEY_DATE, dateFormat.format(lentObject.date))
        args.put(KEY_PERSON, lentObject.personName)
        args.put(KEY_PERSON_KEY, lentObject.personKey)
        return updateLentObject(rowId, args)
    }

    fun markLentObjectAsReturned(rowId: Long): Boolean {
        val values = ContentValues()
        values.put(KEY_BACK, true)
        return updateLentObject(rowId, values)
    }

    fun markReturnedObjectAsLentAgain(rowId: Long): Boolean {
        val values = ContentValues()
        values.put(KEY_BACK, false)
        return updateLentObject(rowId, values)
    }

    private fun updateLentObject(rowId: Long, values: ContentValues): Boolean {
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT)
        values.put(KEY_MODIFICATION_DATE, dateFormat.format(Date()))
        return mDb.update(LENTOBJECTS_DATABASE_TABLE, values, "$KEY_ROWID=$rowId", null) > 0
    }

    companion object {
        const val KEY_DESCRIPTION = "description"
        const val KEY_TYPE = "type"
        const val KEY_DATE = "date"
        const val KEY_MODIFICATION_DATE = "modification_date"
        const val KEY_PERSON = "person"
        const val KEY_PERSON_KEY = "person_key"
        const val KEY_BACK = "back"
        const val KEY_CALENDAR_ENTRY = "calendar_entry"
        const val KEY_ROWID = "_id"
        private var instances: MutableMap<Context, OpenLendDbAdapter> = HashMap()

        /**
         * Database creation sql statement
         */
        private const val LENTOBJECTS_DATABASE_CREATE = ("create table lentobjects (" + KEY_ROWID + " integer primary key autoincrement, "
                + KEY_DESCRIPTION + " text not null, " + KEY_TYPE + " integer, " + KEY_DATE + " date not null, "
                + KEY_MODIFICATION_DATE + " date not null, " + KEY_PERSON + " text not null, " + KEY_PERSON_KEY + " text, "
                + KEY_BACK + " integer not null, " + KEY_CALENDAR_ENTRY + " text);")
        private const val DATABASE_NAME = "data"
        private const val LENTOBJECTS_DATABASE_TABLE = "lentobjects"
        const val DATABASE_VERSION = 4
        private const val CREATE_CALENDAR_ENTRY_COLUMN = "ALTER TABLE $LENTOBJECTS_DATABASE_TABLE ADD COLUMN $KEY_CALENDAR_ENTRY text"
        private const val CREATE_TYPE_COLUMN = "ALTER TABLE $LENTOBJECTS_DATABASE_TABLE ADD COLUMN $KEY_TYPE integer"
        private const val CREATE_MODIFICATION_DATE_COLUMN = "ALTER TABLE $LENTOBJECTS_DATABASE_TABLE ADD COLUMN $KEY_MODIFICATION_DATE date"
        private const val COPY_DATES = "UPDATE $LENTOBJECTS_DATABASE_TABLE SET $KEY_MODIFICATION_DATE = $KEY_DATE"

        @Synchronized
        fun getInstance(ctx: Context): OpenLendDbAdapter {
            return if (!instances.containsKey(ctx)) {
                val instance = OpenLendDbAdapter(ctx)
                instances[ctx] = instance
                instance
            } else {
                instances[ctx]!!
            }
        }
    }
}