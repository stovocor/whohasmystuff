package de.freewarepoint.whohasmystuff.database

import android.net.Uri
import de.freewarepoint.whohasmystuff.LentObject
import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler
import java.util.*

class XMLContentHandler : DefaultHandler() {
    private var databaseVersion = 0
    val lentObjects: MutableList<LentObject> = LinkedList()

    @Throws(SAXException::class)
    override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes) {
        super.startElement(uri, localName, qName, attributes)
        if ("DatabaseBackup" == localName) {
            parseDatabaseBackupAttributes(attributes)
        } else if ("LentObject" == localName) {
            parseLentObjectAttributes(attributes)
        }
    }

    private fun parseDatabaseBackupAttributes(attributes: Attributes) {
        for (i in 0 until attributes.length) {
            val name = attributes.getLocalName(i)
            if ("version" == name) {
                databaseVersion = attributes.getValue(i).toInt()
            }
        }
    }

    private fun parseLentObjectAttributes(attributes: Attributes) {
        var description = ""
        var type = 0
        var date = Date()
        var modificationDate = Date()
        var personName = ""
        var personKey = ""
        var returned = false
        var calendarEventURI: Uri? = null

        for (i in 0 until attributes.length) {
            when (attributes.getLocalName(i)) {
                "description" -> {
                    description = attributes.getValue(i)
                }
                "type" -> {
                    type = attributes.getValue(i).toInt()
                }
                "date" -> {
                    date = Date(attributes.getValue(i).toLong())
                }
                "modificationDate" -> {
                    modificationDate = Date(attributes.getValue(i).toLong())
                }
                "personName" -> {
                    personName = attributes.getValue(i)
                }
                "personKey" -> {
                    personKey = attributes.getValue(i)
                }
                "returned" -> {
                    returned = attributes.getValue(i).toInt() == 1
                }
                "calendarEvent" -> {
                    calendarEventURI = Uri.parse(attributes.getValue(i))
                }
            }
        }
        if (databaseVersion < 4) {
            modificationDate = date
        }

        lentObjects.add(LentObject(description, type, date, modificationDate, personName, personKey,
                returned, calendarEventURI))
    }
}