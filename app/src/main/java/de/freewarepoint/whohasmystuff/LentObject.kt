package de.freewarepoint.whohasmystuff

import android.net.Uri
import android.os.Bundle
import de.freewarepoint.whohasmystuff.database.OpenLendDbAdapter
import java.util.*

data class LentObject(val description: String, val type: Int, val date: Date,
                      var modificationDate: Date, val personName: String, val personKey: String,
                      var returned: Boolean, var calendarEventURI: Uri?) {

    internal constructor(bundle: Bundle) : this(
            bundle.getString(OpenLendDbAdapter.KEY_DESCRIPTION) ?: "",
            bundle.getInt(OpenLendDbAdapter.KEY_TYPE),
            Date(bundle.getLong(OpenLendDbAdapter.KEY_DATE)),
            Date(bundle.getLong(OpenLendDbAdapter.KEY_MODIFICATION_DATE)),
            bundle.getString(OpenLendDbAdapter.KEY_PERSON) ?: "",
            bundle.getString(OpenLendDbAdapter.KEY_PERSON_KEY) ?: "",
            false, null)

}