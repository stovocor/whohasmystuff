package de.freewarepoint.whohasmystuff

import android.content.res.Resources
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class DurationCalculatorTest {

    @Mock
    private val resources: Resources? = null

    private lateinit var durationCalculator: DurationCalculator

    @BeforeEach
    fun setup() {
        durationCalculator = DurationCalculator(resources!!)
    }

    @Test
    fun noTimeDifference() {
        val now = Calendar.getInstance()
        Mockito.`when`(resources!!.getString(R.string.today)).thenReturn("Today")
        val duration = durationCalculator.getTimeDifference(now, now)
        Assertions.assertEquals("Today", duration)
    }

    @Test
    fun yesterdayButNot24Hours() {
        val yesterday = Calendar.getInstance()
        val now = Calendar.getInstance()
        yesterday[2017, 2, 11, 18] = 0
        now[2017, 2, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.yesterday)).thenReturn("Yesterday")
        val duration = durationCalculator.getTimeDifference(yesterday, now)
        Assertions.assertEquals("Yesterday", duration)
    }

    @Test
    fun yesterdayMoreThan24Hours() {
        val yesterday = Calendar.getInstance()
        val now = Calendar.getInstance()
        yesterday[2017, 2, 11, 12] = 0
        now[2017, 2, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.yesterday)).thenReturn("Yesterday")
        val duration = durationCalculator.getTimeDifference(yesterday, now)
        Assertions.assertEquals("Yesterday", duration)
    }

    @Test
    fun twoDaysAgoButNot48Hours() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2017, 2, 10, 18] = 0
        now[2017, 2, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.days)).thenReturn("days")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("2 days", duration)
    }

    @Test
    fun twoDaysAgoMoreThan48Hours() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2017, 2, 10, 12] = 0
        now[2017, 2, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.days)).thenReturn("days")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("2 days", duration)
    }

    @Test
    fun singleWeek() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2017, 3, 1, 12] = 0
        now[2017, 3, 10, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.week)).thenReturn("week")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("1 week", duration)
    }

    @Test
    fun multipleWeeks() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2017, 2, 12, 12] = 0
        now[2017, 3, 10, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.weeks)).thenReturn("weeks")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("4 weeks", duration)
    }

    @Test
    fun singleMonth() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2017, 1, 10, 12] = 0
        now[2017, 2, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.month)).thenReturn("month")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("1 month", duration)
    }

    @Test
    fun nearlyTwoMonths() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2017, 1, 15, 12] = 0
        now[2017, 3, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.month)).thenReturn("month")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("1 month", duration)
    }

    @Test
    fun multipleMonths() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2016, 8, 10, 12] = 0
        now[2017, 1, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.months)).thenReturn("months")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("5 months", duration)
    }

    @Test
    fun singleYear() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2016, 1, 10, 12] = 0
        now[2017, 2, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.year)).thenReturn("year")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("1 year", duration)
    }

    @Test
    fun multipleYears() {
        val before = Calendar.getInstance()
        val now = Calendar.getInstance()
        before[2014, 8, 10, 12] = 0
        now[2017, 1, 12, 15] = 0
        Mockito.`when`(resources!!.getString(R.string.years)).thenReturn("years")
        val duration = durationCalculator.getTimeDifference(before, now)
        Assertions.assertEquals("2 years", duration)
    }

    @Test
    fun inTheFuture() {
        val notBefore = Calendar.getInstance()
        val now = Calendar.getInstance()
        notBefore[2017, 1, 12, 16] = 0
        now[2017, 1, 12, 15] = 0
        val duration = durationCalculator.getTimeDifference(notBefore, now)
        Assertions.assertEquals("0 days", duration)
    }
}