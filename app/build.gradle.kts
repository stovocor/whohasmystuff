plugins {
    id("com.android.application") version "8.5.1"
    id("com.mikepenz.aboutlibraries.plugin") version "11.2.2"
    id("org.jetbrains.kotlin.android") version "2.0.0"
    id("de.mannodermaus.android-junit5") version "1.10.0.0"
}

repositories {
    mavenCentral()
    google()
}

android {

    compileSdk = 34
    namespace = "de.freewarepoint.whohasmystuff"

    defaultConfig {
        applicationId = "de.freewarepoint.whohasmystuff"
        minSdk = 21
        targetSdk = 34
        versionCode = 43
        versionName = "1.1.3"
    }

    if (project.hasProperty("RELEASE_STORE_FILE")) {
        signingConfigs.create("release") {
            storeFile = file(property("RELEASE_STORE_FILE") as String)
            storePassword = property("RELEASE_STORE_PASSWORD") as String
            keyAlias = property("RELEASE_KEY_ALIAS") as String
            keyPassword = property("RELEASE_KEY_PASSWORD") as String
        }
    }

    buildTypes.named("release") {
        isMinifyEnabled = false
        if (project.hasProperty("RELEASE_STORE_FILE")) {
            signingConfig = signingConfigs.findByName("release")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    lint {
        abortOnError = false
    }

    dependencies {
        implementation(group = "androidx.legacy", name = "legacy-support-v4", version = "1.0.0")
        implementation(
            group = "com.mikepenz",
            name = "aboutlibraries",
            version = "11.2.2"
        )
        testImplementation(
            group = "org.junit.jupiter",
            name = "junit-jupiter-api",
            version = "5.10.3"
        )
        testRuntimeOnly(
            group = "org.junit.jupiter",
            name = "junit-jupiter-engine",
            version = "5.10.3"
        )
        testImplementation(group = "org.assertj", name = "assertj-core", version = "3.26.0")
        testImplementation(
            group = "org.mockito",
            name = "mockito-junit-jupiter",
            version = "5.12.0"
        )
    }
}