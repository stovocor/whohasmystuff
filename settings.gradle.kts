pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
    }
    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
                "com.android.application" -> {
                    useModule("com.android.tools.build:gradle:${requested.version}")
                }
                "com.google.android.gms.oss-licenses-plugin" -> {
                    useModule("com.google.android.gms:oss-licenses-plugin:${requested.version}")
                }
                "de.mannodermaus.android-junit5" -> {
                    useModule("de.mannodermaus.gradle.plugins:${requested.id.name}:${requested.version}")
                }
            }
        }
    }
}

include(":app")
rootProject.name = "Who Has My Stuff"